<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>CulturePedia</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Steel Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<style type="text/css" media="screen">
	/* <![CDATA[*/
	@import url(<?php echo $base_url; ?>css/bootstrap.css);
	@import url(<?php echo $base_url; ?>css/style.css);
	@import url(<?php echo $base_url; ?>css/lightbox.css);
	/*]]>*/
</style>
<!-- js -->
<script type="text/javascript" src="<?= base_url() ?>js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<!--web-fonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Dosis:400,200,300,500,600,700,800' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--circle-chart-->
<script type="text/javascript" src="<?= base_url() ?>js/jquery.circlechart.js"></script>
<!--circle-chart-->
</head>
<body>
	<!--header-->
	<div class="header">
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header navbar-left">
					<h1><a href="">CULTURE <span>ENCYCLOPEDIA</span></a></h1>
				</div>
				<!--navigation-->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="header-right">
					
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">					
						<ul class="nav navbar-nav navbar-left cl-effect-14">
							<li><a href="<?php echo site_url('traditional_culture_control/loginlink');?>" class="active">Login</a></li>
							<li><a href="<?php echo site_url('traditional_culture_control/signuplink');?>" class="active">SignUp</a></li>		
						</ul>		
					</div><!--//navigation-->
				</div>
			</div>	
		</nav>		
	</div>	
	<!--//header-->
	<!--banner-->
	<div class="banner">
		<div class="container">
			<div class="banner-text">
				<div  id="top" class="callbacks_container">
					<ul class="rslides" id="slider3">
						<li>
							<div class="banner-text-info">
								<h2>WELCOME TO CULTUREPEDIA</h2>	
								<h4>We Expand your knowledge of many Culture around World</h4>	
							</div>	
						</li>
						<li>
							<div class="banner-text-info">
								<h3>GET YOUR CULTURE API !</h3>		
								<h4>Access our Culture Data with just one click sign in!</h4>	
							</div>	
						</li>
						<li>
							<div class="banner-text-info">
								<h3>CONFUSED HOW TO USE ?</h3>		
								<h4>Don't worry we provide you a guide to use</h4>	
							</div>		
						</li>
						<li>
							<div class="banner-text-info">
								<h3>FEEL SOMETHING MISSING ?</h3>		
								<h4>Feel free to send us a Feedback, at plbtwg@mail.com</h4>	
							</div>		
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--modal-sign-->
	<div class="modal bnr-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body modal-spa">
						<img class="img-responsive" src="images/slid.jpg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras rutrum iaculis enim, non convallis felis mattis at. Donec fringilla lacus eu pretium rutrum. Cras aliquet congue ullamcorper. Etiam mattis eros eu ullamcorper volutpat. Proin ut dui a urna efficitur varius. uisque molestie cursus mi et congue consectetur adipiscing elit cras rutrum iaculis enim, Lorem ipsum dolor sit amet, non convallis felis mattis at. Maecenas sodales tortor ac ligula ultrices dictum et quis urna. Etiam pulvinar metus neque, eget porttitor massa vulputate in. Fusce lacus purus, pulvinar ut lacinia id, sagittis eu mi. Vestibulum eleifend massa sem, eget dapibus turpis efficitur at. Aliquam viverra quis leo et efficitur. Nullam arcu risus, scelerisque quis interdum eget, fermentum viverra turpis. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut At vero eos </p>
					</div>
				</section>
			</div>
		</div>
	</div>
	<!--//modal-sign-->
	<!--//banner-->
	<!--about-->
	<div class="about">
		<div class="container">
			<h3 class="title">About Us </h3>
			<div class="about-text">
				<h4>We provide your Culture Knowledge through our data</h4>
				<p>CulturePedia provide so many Culture data around the world for you to use in your Web or Application. Easy to use and 
			    complete guide for your first time accessing API. CulturePedia founded by Adityo, Edho, Galang, Niko and Pier to complete final project of PLBTW course.
				We hope that the data provided by us would be convenient and usefull for Application or Web use. We also provide apps for data gathering you can download here at
				<a href="#">http://www.google.com</a></p>
			</div>
		</div>
	</div>
	<!--//about-->
	<!--footer-->
	<div class="footer">
		<div class="container">
			<p>© 2016 PLBTW - Traditional Culture | Design by <a href="http://w3layouts.com">W3layouts</a></p>
		</div>
	</div>
	<!--//footer-->		
	<!--script for portfolio-->
	<script type="text/javascript" src="<?= base_url() ?>js/lightbox-plus-jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>js/easyResponsiveTabs.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
		});		
	</script>
	<!--//script for portfolio-->
	<!--banner Slider starts Here-->
	<script type="text/javascript" src="<?= base_url() ?>js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
		  // Slideshow 3
		  $("#slider3").responsiveSlides({
			auto: true,
			pager: true,
			nav: false,
			speed: 500,
			namespace: "callbacks",
			before: function () {
			  $('.events').append("<li>before event fired.</li>");
			},
			after: function () {
			  $('.events').append("<li>after event fired.</li>");
			}
		  });
	
		});
	</script>
	<!--//End-slider-script-->
	<!-- start-smooth-scrolling-->
	<script type="text/javascript" src="<?= base_url() ?>js/move-top.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!--//end-smooth-scrolling-->	
	<!--smooth-scrolling-of-move-up-->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!--//smooth-scrolling-of-move-up-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="<?= base_url() ?>js/bootstrap.js"></script>
</body>
</html>
