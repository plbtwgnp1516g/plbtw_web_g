<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>	
<head>
<title>CulturePedia - SignUp</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<meta name="keywords" content="Transparent Organic Form Responsive Templates, Iphone Widget Template, Smartphone login forms,Login form, Widget Template, Responsive Templates, a Ipad 404 Templates, Flat Responsive Templates" />
<style type="text/css" media="screen">
	/* <![CDATA[*/
	@import url(<?php echo $base_url; ?>css/style-login-signup.css);
	/*]]>*/
</style
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--/web-fonts-->
</head>
<body>
	<h1><u>CulturePedia Registration</u></h1>
<div class="two-forms">
	<div class="wrap">
		<div class="signup">
			<div class="singup-info">
						<form method="post" action="<?php echo site_url('traditional_culture_control/signupnow');?>" name="form">
							<input type="text" class="text" name="firstname" id="firstname" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}" >
								<input type="text" class="text" name="secondname" id="secondname" value="Second Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Second Name';}" >
									<div class="users">	
										<input type="text" class="text" name="username" id="username" value="User Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'User Name';}" >
										<input type="password" name="password" id="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}">
									</div>
								<input id="signupbutton" type="submit" value="signup" >
								<div id="salah"><strong>Error : <?php echo $error_message;?></strong></div>
						</form>
						<div class="clear"> </div>
						<p>Already have an Account ?<a href="<?php echo site_url('traditional_culture_control/loginlink');?>">Login here</a> or<a href="<?php echo site_url('traditional_culture_control/index');?>">Back</a></p>
						<div class="clear"> </div>
			</div>
		</div>
		<div class="clear"> </div>
	</div>
</div>
		<div class="copy-right">
					<p>Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a> </p>
			</div>

</body>
</html>