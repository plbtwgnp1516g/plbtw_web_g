<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>	
<head>
<title>Flate Signup And Login Form with Flat Responsive template :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<meta name="keywords" content="Transparent Organic Form Responsive Templates, Iphone Widget Template, Smartphone login forms,Login form, Widget Template, Responsive Templates, a Ipad 404 Templates, Flat Responsive Templates" />

<style type="text/css" media="screen">
	/* <![CDATA[*/
	@import url(<?php echo $base_url; ?>css/style-login-signup.css);
	/*]]>*/
</style
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--/web-fonts-->
</head>
<body>
	<h1>CulturePedia Login </h1>
<div class="two-forms">
	<div class="wrap">
		<div class="login">
			<div class="login-info">
				<form method="post" action="<?php echo site_url('traditional_culture_control/login');?>" name="form">
					<input type="text" name="username" id="username" class="text" value="User Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'User Name';}" >
					<input type="password" name="password" id="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}">
					<div class="btns">
					<input type="submit" value="login" ></div>
				</form>
				<p>Not have an Account ?<a href="<?php echo site_url('traditional_culture_control/signuplink');?>"><span>Click here</span></a> or<a href="<?php echo site_url('traditional_culture_control/index');?>"><span>Back</span></a></p>
			</div>
		</div>
		<div class="clear"> </div>
	</div>
</div>
		<div class="copy-right">
					<p>Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a> </p>
			</div>

</body>
</html>