<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Traditional_culture_control extends CI_Controller{
	function __construct()
	{
		parent::__construct();	
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('traditional_culture_model');
		$this->load->library('pagination');
		$this->load->library('image_lib');
		$this->load->helper('string');
		$this->load->library('session');
	}
	
	function index()
	{
		$data['base_url'] = $this->config->item('base_url');
		$this->load->view('dashboard',$data);
	}
	
	function HomeLogged(){
		$data['base_url'] = $this->config->item('base_url');
		$this->load->view('dashboard - logged_in',$data);
	}
	
	function signuplink()
	{
		$data['base_url'] = $this->config->item('base_url');
		$this->load->view('signup',$data);
	}
	
	function loginlink(){
		$data['base_url'] = $this->config->item('base_url');
		$this->load->view('login',$data);
	}

	function signupnow()
	{
		$data['base_url'] = $this->config->item('base_url');
		$firstname = $this->input->post('firstname');
		$secondname = $this->input->post('secondname');
		$fullname = $firstname.' '.$secondname;
		$password = $this->input->post('password');
		$username = $this->input->post('username');
		$apikey = random_string('alnum', 32);
		
		$cekusername = $this->traditional_culture_model->cekusername($username);
		
		if($firstname == 'First Name' or $secondname == 'Second Name' or $password == 'Password' or $username == 'User Name'){
			$data['error_message']='Please fill in the Blank';
			$this->load->view('signup_salah_input',$data);
		}else{
			if($cekusername == false){
				$data['error_message']='Username already Used';
				$this->load->view('signup_salah_input',$data);
			}else{
				$this->traditional_culture_model->signupuser($username,$password,$fullname,$apikey);
				$this->load->view('dashboard',$data);	
			}
		}
	}
	
	function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data['base_url'] = $this->config->item('base_url');
		
		$resultquery=$this->traditional_culture_model->login($username,$password);
		
		if($resultquery == false)
		{
			$data['error_message']='User not Found';
			$this->load->view('login_salah',$data);
		}
		else
		{
			$this->session->set_userdata('loggeddata',$resultquery);
			redirect('traditional_culture_control/getloginsession');	
		}
	}
	
	function getloginsession(){	
		if($this->session->userdata('loggeddata')){
			$session_data = $this->session->userdata('loggeddata');
			$data['fullname'] = $session_data['fullname'];
			$data['apikey'] = $session_data['apikey'];
			$data['base_url'] = $this->config->item('base_url');
			$this->load->view('dashboard - logged_in',$data);
		}else{
			redirect('traditional_culture_control/index');
		}
	}
	
	function logout()
	{
	  $this->session->unset_userdata(array('loggeddata' => ''));
	  redirect('traditional_culture_control/index');
	}
}
?>