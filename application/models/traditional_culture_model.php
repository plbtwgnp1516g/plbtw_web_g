<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Traditional_culture_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function login($username,$password)
	{
	
		$this->db->select('username,password');
		$this->db->from('user_api');
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		
		$query = $this->db->get();
		
		if($query ->num_rows()==1)
		{	
			$this->db->select('*');
			$this->db->from('user_api');
			$this->db->where('username',$username);
			$this->db->where('password',$password);
			
			$queryresult = $this->db->get();
			
			$resultdata = array(
				'fullname'=>$queryresult->row("fullname"),
				'apikey'=>$queryresult->row("API_key"),
			);
			
			return $resultdata;
		}
		else
		{
			return false;
		}
	}
	
	function signupuser($username,$password,$fullname,$apikey)
	{
		$data = array(
				'username' => $username,
				'password' => $password,
				'fullname' => $fullname,
				'api_key' => $apikey,
			);
		$this->db->insert('user_api',$data);
	}
	
	function cekusername($username)
	{
		$this->db->select('username');
		$this->db->from('user_api');
		$this->db->where('username',$username);
		
		$query = $this->db->get();
		if($query ->num_rows()==1)
		{	
			return false;	
		}
		else
		{
			return true;
		}
	}
}